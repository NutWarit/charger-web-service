package com.charger.webservice.DTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ConnectorDTO {

    private Integer id;
    private Integer chargerId;
    private String connectorType;
    private Integer connectorNo;
    private Double connectorRateKw;
    private Double connectorRatePerMin;
    private Double connectorRatePrice;
    private Double connectorRateHours;
    private Double connectorRateStart;
    private String qrCodeToken;
    private String qrCodeImageUrl;
    private String chargeStationId;

}
