package com.charger.webservice.DTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PaymentTransactionDTO {

    private Integer id;
    private Integer chargerTransactionId;
    private Double totalCost;
    private Double totalRateKw;
    private Double totalRatePerMin;
    private Double totalTime;
    private String paymentSource;
    private String paymentTransactionType;
    private String paymentTransactionCost;
    private String paymentStatus;

}
