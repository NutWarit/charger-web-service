package com.charger.webservice.DTO;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;

@Getter
@Setter
public class ChargerDTO {

    private Integer id;
    private Integer chargerStatusId;
    private Integer locationId;
    private String chargerImageUrl;
    private String name;
    private Double chargerRateKw;
    private Double chargerRatePerMin;
    private String status;

}
