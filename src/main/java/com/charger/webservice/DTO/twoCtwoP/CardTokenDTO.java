package com.charger.webservice.DTO.twoCtwoP;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CardTokenDTO {

    private String paymentToken;
    private String locale;
    private String clientID;
}
