package com.charger.webservice.DTO.twoCtwoP;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PaymentTokenDTO {

    private String payload;
}
