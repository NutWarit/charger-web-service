package com.charger.webservice.DTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BillPageDTO {

    private String webPaymentUrl;
}
