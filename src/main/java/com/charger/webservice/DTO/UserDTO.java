package com.charger.webservice.DTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDTO {

    private Integer id;
    private String username;
    private String password;
    private String email;
    private String profileImageUrl;
    private String firstName;
    private String lastName;
    private String cardNumber;
    private String phoneNumber;
    private String gender;
    private Integer age;
    private boolean isKyc;
    private boolean isOpt;
    private boolean isAccept;

}
