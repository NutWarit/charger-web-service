package com.charger.webservice.DTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PasswordDTO {

    private String token;
    private String oldPassword;
    private String password;
    private String rePassword;
    private Integer userId;

}
