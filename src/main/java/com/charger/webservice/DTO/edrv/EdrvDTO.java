package com.charger.webservice.DTO.edrv;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EdrvDTO {

    private String sessionId;
}
