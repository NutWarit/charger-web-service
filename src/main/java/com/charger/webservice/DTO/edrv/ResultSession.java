package com.charger.webservice.DTO.edrv;

import com.fasterxml.jackson.annotation.*;
import com.google.zxing.Result;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "_id",
        "user",
        "chargestation",
        "connector",
        "status"
})
public class ResultSession {

    @JsonProperty("_id")
    public String _id;
    @JsonProperty("user")
    public String user;
    @JsonProperty("chargestation")
    public String chargestation;
    @JsonProperty("connector")
    public String connector;
    @JsonProperty("status")
    public String status;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonProperty("_id")
    public String get_id() {
        return _id;
    }

    @JsonProperty("_id")
    public void set_id(String _id) {
        this._id = _id;
    }

    @JsonProperty("user")
    public String getUser() {
        return user;
    }

    @JsonProperty("user")
    public void setUser(String user) {
        this.user = user;
    }

    @JsonProperty("chargestation")
    public String getChargestation() {
        return chargestation;
    }

    @JsonProperty("chargestation")
    public void setChargestation(String chargestation) {
        this.chargestation = chargestation;
    }

    @JsonProperty("connector")
    public String getConnector() {
        return connector;
    }

    @JsonProperty("connector")
    public void setConnector(String connector) {
        this.connector = connector;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
