package com.charger.webservice.DTO.edrv;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "connectors"
})
public class ResultChargeStations {

    @JsonProperty("connectors")
    private List<ConnectorChargeStasions> connectors;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonProperty("connectors")
    public List<ConnectorChargeStasions> getConnectors() {
        return connectors;
    }

    @JsonProperty("connectors")
    public void setConnectors(List<ConnectorChargeStasions> connectors) {
        this.connectors = connectors;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
