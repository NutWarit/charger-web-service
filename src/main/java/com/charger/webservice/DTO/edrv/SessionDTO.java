package com.charger.webservice.DTO.edrv;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SessionDTO {

    private String user;
    private String connector;

}
