package com.charger.webservice.DTO.edrv;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "_id",
        "power_type"
})
public class ConnectorChargeStasions {

    @JsonProperty("_id")
    private String _id;
    @JsonProperty("power_type")
    private String power_type;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonProperty("_id")
    public String get_id() {
        return _id;
    }

    @JsonProperty("_id")
    public void set_id(String _id) {
        this._id = _id;
    }

    @JsonProperty("power_type")
    public String getPower_type() {
        return power_type;
    }

    @JsonProperty("power_type")
    public void setPower_type(String power_type) {
        this.power_type = power_type;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
