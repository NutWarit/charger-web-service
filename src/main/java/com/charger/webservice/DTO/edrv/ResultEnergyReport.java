package com.charger.webservice.DTO.edrv;

import com.fasterxml.jackson.annotation.*;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "timestamp",
        "energy_meter"
})
public class ResultEnergyReport {

    @JsonProperty("timestamp")
    public Timestamp timestamp;

    @JsonProperty("energy_meter")
    private EnergyMeter energy_meter;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonProperty("timestamp")
    public Timestamp getTimestamp() {
        return timestamp;
    }

    @JsonProperty("timestamp")
    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    @JsonProperty("energy_meter")
    public EnergyMeter getEnergy_meter() {
        return energy_meter;
    }

    @JsonProperty("energy_meter")
    public void setEnergy_meter(EnergyMeter energy_meter) {
        this.energy_meter = energy_meter;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
