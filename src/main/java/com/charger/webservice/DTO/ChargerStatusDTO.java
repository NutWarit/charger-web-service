package com.charger.webservice.DTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChargerStatusDTO {

    private Integer id;
    private Boolean alwaysOpenFlag;
    private String fromHours;
    private String fromMinutes;
    private String toHours;
    private String toMinutes;

}
