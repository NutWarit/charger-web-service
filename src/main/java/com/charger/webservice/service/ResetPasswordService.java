package com.charger.webservice.service;

import com.charger.webservice.DTO.PasswordDTO;
import com.charger.webservice.DTO.UserDTO;
import com.charger.webservice.entity.ResetPassword;
import com.charger.webservice.entity.user.User;
import com.charger.webservice.exceptions.BadRequestException;
import com.charger.webservice.repository.ResetPasswordRepository;
import com.charger.webservice.repository.UserRepository;
import javassist.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.DelegatingPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.security.SecureRandom;
import java.util.*;

@Service
public class ResetPasswordService {
    private static final Logger logger = LoggerFactory.getLogger(ResetPasswordService.class);

    protected static SecureRandom random = new SecureRandom();
    @Value("${password.reset-token-timeout}")
    private Long resetTokenTimeout;
    @Value("${url.forgot}")
    private String urlForgot;
    @Autowired
    private ResetPasswordRepository resetPasswordRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private EmailService emailService;
    @Autowired
    private DelegatingPasswordEncoder delegatingPasswordEncoder;

    public void requestResetPassword(UserDTO dto) {

        try {
            if (dto.getEmail() == null && dto != null) {
                throw new BadRequestException(HttpStatus.BAD_REQUEST, "Invalid Email");
            }

            User user = this.userRepository.findByEmail(dto.getEmail());

            // no email no user
            if (user == null) {
                throw new NotFoundException("Email not Found");
            }
            
            // Generate a OTP
            Date date = new Date();
            ResetPassword resetPasswordToken = new ResetPassword();
            resetPasswordToken.setUserId(user.getId());
            resetPasswordToken.setToken(UUID.randomUUID().toString());
            resetPasswordToken.setExpiredDate(new Date(date.getTime() + resetTokenTimeout));
            resetPasswordToken = this.resetPasswordRepository.save(resetPasswordToken);

            //Send OTP to email
            String username = null;
            Map<String, Object> data = new TreeMap<>();
            data.put("username", username);
            data.put("urlForgot", urlForgot);
            data.put("hash", resetPasswordToken.getToken());
            List<String> emails = new ArrayList<>();
            emails.add(dto.getEmail());
            emailService.sendEmail("reset_password.html", "ตั้งรหัสผ่านใหม่", data, emails, null, null);
        } catch (Exception e) {
            logger.error( "requestResetPassword error : " + e.getMessage());
            throw new BadRequestException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

    @Transactional
    public String resetPassword(PasswordDTO dto) {
        ResetPassword resetPasswordToken = this.resetPasswordRepository.findOneByToken(dto.getToken());

        // wrong token                     // expired token
        if (resetPasswordToken == null || !checkResetPasswordTokenExpiration(dto.getToken())) {
            throw new BadRequestException(HttpStatus.BAD_REQUEST, "Token is not valid");
        }

        if ((dto.getPassword().length() < 8 || dto.getPassword().length() > 32)) {
            throw new BadRequestException(HttpStatus.BAD_REQUEST, "PASSWORD_FORMAT_INVALID");
        }

        if (!(dto.getPassword().matches(".*[a-z].*") && dto.getPassword().matches(".*[0-9].*") && dto.getPassword().matches(".*[A-Z].*"))) {
            throw new BadRequestException(HttpStatus.BAD_REQUEST, "PASSWORD_FORMAT_INVALID");
        }

        if (!dto.getPassword().equals(dto.getRePassword())) {
            throw new BadRequestException(HttpStatus.BAD_REQUEST, "Password is not match.");
        }

        User user = this.userRepository.findOneById(resetPasswordToken.getUserId());
        this.changePassword(user.getId(), dto.getPassword());
        this.resetPasswordRepository.delete(resetPasswordToken);

        return "OK";
    }

    public Boolean checkResetPasswordTokenExpiration(String token) {
        ResetPassword resetPasswordToken = this.resetPasswordRepository.findOneByToken(token);
        // wrong token
        if (resetPasswordToken == null) {
            throw new BadRequestException(HttpStatus.BAD_REQUEST, "Token is not valid");
        }

        Date date = new Date();
        if (resetPasswordToken.getExpiredDate().getTime() > date.getTime()) {
            // token valid
            return true;
        } else {
            // token expired
            this.resetPasswordRepository.delete(resetPasswordToken);
            return false;
        }
    }

    public void changePassword(Integer userId, String newPassword) {
        User user = this.userRepository.findOneById(userId);
        user.setPassword(this.delegatingPasswordEncoder.encode(newPassword));
//        user.setInvalidAuthCount(0);
        user.setNonLocked(true);
        this.userRepository.save(user);
    }

    public String changePassword(
            PasswordDTO dto,
            Authentication authentication
    ) {
        User u = (User) authentication.getPrincipal();
        User user = userRepository.findOneById(u.getId());
        if (!delegatingPasswordEncoder.matches(dto.getOldPassword(), user.getPassword())) {
            throw new BadRequestException(HttpStatus.BAD_REQUEST, "Wrong password.");
        }
        if (!dto.getPassword().equals(dto.getRePassword())) {
            throw new BadRequestException(HttpStatus.BAD_REQUEST, "Password is not match.");
        }
        if ((dto.getPassword().length() < 6 || dto.getPassword().length() > 32)) {
            throw new BadRequestException(HttpStatus.BAD_REQUEST, "Password must be 6 - 32 alphanumeric characters.");
        }
        if (delegatingPasswordEncoder.matches(dto.getPassword(), user.getPassword())) {
            throw new BadRequestException(HttpStatus.BAD_REQUEST, "Password is same previous.");
        }
        //    if(!StringUtils.isAlphanumeric(newPassword) || (newPassword.length() < 6 || newPassword.length() > 32)){
//        throw new BadRequestException(HttpStatus.BAD_REQUEST, "Password must be 6 - 32 alphanumeric characters.");
//    }
        user.setPassword(this.delegatingPasswordEncoder.encode(dto.getPassword()));
        userRepository.saveAndFlush(user);
        return "OK";
    }

    public String adminChangePassword(PasswordDTO dto) {

        if ((dto.getPassword().length() < 6 || dto.getPassword().length() > 32)) {
            throw new BadRequestException(HttpStatus.BAD_REQUEST, "Password must be 6 - 32 alphanumeric characters.");
        }

        if (!dto.getPassword().equals(dto.getRePassword())) {
            throw new BadRequestException(HttpStatus.BAD_REQUEST, "Password is not match.");
        }

        User user = this.userRepository.findOneById(dto.getUserId());
        if (delegatingPasswordEncoder.matches(dto.getPassword(), user.getPassword())) {
            throw new BadRequestException(HttpStatus.BAD_REQUEST, "Password is same previous.");
        }
        this.changePassword(user.getId(), dto.getRePassword());

        return "OK";

    }
}
