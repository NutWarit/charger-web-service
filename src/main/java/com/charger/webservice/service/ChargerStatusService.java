package com.charger.webservice.service;

import com.charger.webservice.DTO.ChargerStatusDTO;
import com.charger.webservice.entity.ChargerStatus;
import com.charger.webservice.entity.user.User;
import com.charger.webservice.exceptions.BadRequestException;
import com.charger.webservice.exceptions.CustomHTTPGenericException;
import com.charger.webservice.exceptions.UnauthorizedException;
import com.charger.webservice.repository.ChargerStatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Date;
import java.util.List;

@Service
public class ChargerStatusService {

    @Autowired
    private ChargerStatusRepository chargerStatusRepository;

    @PersistenceContext
    private EntityManager entityManager;

    public PageImpl<ChargerStatus> list(Integer page, Integer size, Authentication authentication) {

        User user = (User) authentication.getPrincipal();
        if (user == null) {
            throw new CustomHTTPGenericException(HttpStatus.UNAUTHORIZED, "Not Authorized");
        }

        StringBuffer strSelectQuery = new StringBuffer("select cs from ChargerStatus cs where 1=1");
        StringBuffer strCountQuery = new StringBuffer("SELECT count(cs.id) FROM ChargerStatus cs where 1=1");

        Query query = entityManager.createQuery(strSelectQuery.toString());
        query.setFirstResult((page - 1) * size);
        query.setMaxResults(size);
        Query queryCount = entityManager.createQuery(strCountQuery.toString());

        long countResults = (long) queryCount.getSingleResult();

        List<ChargerStatus> chargerStatusList = query.getResultList();

        PageImpl<ChargerStatus> pageResult = new PageImpl<ChargerStatus>(chargerStatusList, PageRequest.of(page - 1, size), countResults);

        return pageResult;
    }

    public ChargerStatusDTO get(Integer statusId,
                                Authentication authentication
    ) {
        ChargerStatus chargerStatus = chargerStatusRepository.findOneById(statusId);
        if (chargerStatus == null) {
            throw new BadRequestException(HttpStatus.BAD_REQUEST, "Not found Id");
        }

        User user = (User) authentication.getPrincipal();
        if (user == null) {
            throw new UnauthorizedException(HttpStatus.UNAUTHORIZED, "Not Authorized");
        }

        ChargerStatusDTO dto = new ChargerStatusDTO();
        dto.setId(chargerStatus.getId());
        dto.setAlwaysOpenFlag(chargerStatus.getAlwaysOpenFlag());
        dto.setFromHours(chargerStatus.getFromHours());
        dto.setFromMinutes(chargerStatus.getFromMinutes());
        dto.setToHours(chargerStatus.getToHours());
        dto.setToMinutes(chargerStatus.getToMinutes());

        return dto;

    }

    public ChargerStatus create(ChargerStatusDTO dto,Authentication authentication) {

        User user = (User) authentication.getPrincipal();
        if (user == null) {
            throw new UnauthorizedException(HttpStatus.UNAUTHORIZED, "Not Authorized");
        }

        ChargerStatus chargerStatus = new ChargerStatus();
        chargerStatus.setAlwaysOpenFlag(dto.getAlwaysOpenFlag());
        chargerStatus.setFromHours(dto.getFromHours());
        chargerStatus.setFromMinutes(dto.getFromMinutes());
        chargerStatus.setToHours(dto.getToHours());
        chargerStatus.setToMinutes(dto.getToMinutes());

        chargerStatus.setCreatedBy(user.getUsername());
        chargerStatus.setCreatedDate(new Date());
        chargerStatus.setUpdatedBy(user.getUsername());
        chargerStatus.setUpdatedDate(new Date());

        chargerStatusRepository.save(chargerStatus);

        return chargerStatus;

    }

    public ChargerStatus update(ChargerStatusDTO dto,Authentication authentication) {

        User user = (User) authentication.getPrincipal();
        if (user == null) {
            throw new UnauthorizedException(HttpStatus.UNAUTHORIZED, "Not Authorized");
        }

        ChargerStatus chargerStatus = chargerStatusRepository.findOneById(dto.getId());

        chargerStatus.setAlwaysOpenFlag(dto.getAlwaysOpenFlag());
        chargerStatus.setFromHours(dto.getFromHours());
        chargerStatus.setFromMinutes(dto.getFromMinutes());
        chargerStatus.setToHours(dto.getToHours());
        chargerStatus.setToMinutes(dto.getToMinutes());

        chargerStatus.setUpdatedBy(user.getUsername());
        chargerStatus.setUpdatedDate(new Date());

        chargerStatusRepository.save(chargerStatus);

        return chargerStatus;

    }

    public void delete(
            Integer statusId,
            Authentication authentication
    ){
        User user = (User) authentication.getPrincipal();

        if (user == null) {
            throw new BadRequestException(HttpStatus.UNAUTHORIZED, "User is UNAUTHORIZED");
        }
        chargerStatusRepository.deleteById(statusId);
    }
}
