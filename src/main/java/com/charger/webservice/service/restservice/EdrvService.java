package com.charger.webservice.service.restservice;

import com.charger.webservice.DTO.ConnectorDTO;
import com.charger.webservice.DTO.edrv.*;
import com.charger.webservice.exceptions.CustomHTTPGenericException;
import com.charger.webservice.util.Constants;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;

@Service
public class EdrvService {

    @Value("${edrv.api.url.session}")
    private String urlSession;
    @Value("${edrv.api.url.chargestations}")
    private String urlChargestations;
    @Value("${edrv.api.url.user}")
    private String urlUser;
    @Value("${edrv.api.token}")
    private String token;

    public String edrvSession(SessionDTO dto) {

        RestTemplate restTemplate = new RestTemplate();
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
        CloseableHttpClient httpClient = HttpClientBuilder.create()
                .setRedirectStrategy(new LaxRedirectStrategy())
                .build();
        factory.setHttpClient(httpClient);
        restTemplate.setRequestFactory(factory);

        String responsebody = null;

        // TODO: 28/1/2021 AD change value from hard code "x-api-key" and "URI" to @value in application.properties
        HttpHeaders headers = new HttpHeaders();
        MediaType mediaType = new MediaType("application", "json", Charset.forName("UTF-8"));
        headers.set("Authorization", "Bearer "+token);
        headers.setContentType(mediaType);

        ObjectMapper objectMapper = new ObjectMapper();
        String value = null;
        try {
            value = objectMapper.writeValueAsString(dto);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        HttpEntity<String> entity = new HttpEntity<String>(value, headers);

        ResponseEntity<String> response = null;
        try {
            response = restTemplate.exchange(new URI(
                            urlSession),
                    HttpMethod.POST,
                    entity,
                    String.class);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        if (response.getStatusCode() == HttpStatus.OK) {
            responsebody = response.getBody();
        } else {
            throw new CustomHTTPGenericException(HttpStatus.INTERNAL_SERVER_ERROR, Constants.MESSAGE_CODE_ERRORS.REDEEM_ERROR);
        }
        return responsebody;

    }

    public String edrvSessionStop(EdrvDTO dto) {

        RestTemplate restTemplate = new RestTemplate();
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
        CloseableHttpClient httpClient = HttpClientBuilder.create()
                .setRedirectStrategy(new LaxRedirectStrategy())
                .build();
        factory.setHttpClient(httpClient);
        restTemplate.setRequestFactory(factory);

        String responsebody = null;

        // TODO: 28/1/2021 AD change value from hard code "x-api-key" and "URI" to @value in application.properties
        HttpHeaders headers = new HttpHeaders();
        MediaType mediaType = new MediaType("application", "json", Charset.forName("UTF-8"));
        headers.set("Authorization", "Bearer "+token);
        headers.setContentType(mediaType);

        HttpEntity<String> entity = new HttpEntity<String>(headers);

        String id = dto.getSessionId();
        ResponseEntity<String> response = null;
        try {
            response = restTemplate.exchange(new URI(
                            urlSession + "/" + id + "/stop"),
                    HttpMethod.GET,
                    entity,
                    String.class);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        if (response.getStatusCode() == HttpStatus.OK) {
            responsebody = response.getBody();
        } else {
            throw new CustomHTTPGenericException(HttpStatus.INTERNAL_SERVER_ERROR, Constants.MESSAGE_CODE_ERRORS.REDEEM_ERROR);
        }
        return responsebody;

    }

    public String getSessionId(RootSession responseSession) {

        String sessionId = responseSession.getResult().get_id();

        return sessionId;
    }

    public String edrvSessionEnergyReport(EdrvDTO dto) {

        RestTemplate restTemplate = new RestTemplate();
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
        CloseableHttpClient httpClient = HttpClientBuilder.create()
                .setRedirectStrategy(new LaxRedirectStrategy())
                .build();
        factory.setHttpClient(httpClient);
        restTemplate.setRequestFactory(factory);

        String responsebody = null;

        // TODO: 28/1/2021 AD change value from hard code "x-api-key" and "URI" to @value in application.properties
        HttpHeaders headers = new HttpHeaders();
        MediaType mediaType = new MediaType("application", "json", Charset.forName("UTF-8"));
        headers.set("Authorization", "Bearer "+token);
        headers.setContentType(mediaType);

        HttpEntity<String> entity = new HttpEntity<String>(headers);

        String id = dto.getSessionId();

        ResponseEntity<String> response = null;
        try {
            response = restTemplate.exchange(new URI(
                            urlSession + "/" + id + "/energy_reports"),
                    HttpMethod.GET,
                    entity,
                    String.class);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        if (response.getStatusCode() == HttpStatus.OK) {
            responsebody = response.getBody();
        } else {
            throw new CustomHTTPGenericException(HttpStatus.INTERNAL_SERVER_ERROR, Constants.MESSAGE_CODE_ERRORS.REDEEM_ERROR);
        }
        return responsebody;

    }

    public String edrvSessionEnergyReportStart(String sessionId) {

        RestTemplate restTemplate = new RestTemplate();
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
        CloseableHttpClient httpClient = HttpClientBuilder.create()
                .setRedirectStrategy(new LaxRedirectStrategy())
                .build();
        factory.setHttpClient(httpClient);
        restTemplate.setRequestFactory(factory);

        String responsebody = null;

        // TODO: 28/1/2021 AD change value from hard code "x-api-key" and "URI" to @value in application.properties
        HttpHeaders headers = new HttpHeaders();
        MediaType mediaType = new MediaType("application", "json", Charset.forName("UTF-8"));
        headers.set("Authorization", "Bearer "+token);
        headers.setContentType(mediaType);

        HttpEntity<String> entity = new HttpEntity<String>(headers);

        String id = sessionId;

        ResponseEntity<String> response = null;
        try {
            response = restTemplate.exchange(new URI(
                            urlSession + "/" + id + "/energy_reports"),
                    HttpMethod.GET,
                    entity,
                    String.class);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        if (response.getStatusCode() == HttpStatus.OK) {
            responsebody = response.getBody();
        } else {
            throw new CustomHTTPGenericException(HttpStatus.INTERNAL_SERVER_ERROR, Constants.MESSAGE_CODE_ERRORS.REDEEM_ERROR);
        }
        return responsebody;

    }

    public String edrvChargestationsById(ConnectorDTO dto) {

        RestTemplate restTemplate = new RestTemplate();
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
        CloseableHttpClient httpClient = HttpClientBuilder.create()
                .setRedirectStrategy(new LaxRedirectStrategy())
                .build();
        factory.setHttpClient(httpClient);
        restTemplate.setRequestFactory(factory);

        String responsebody = null;

        // TODO: 28/1/2021 AD change value from hard code "x-api-key" and "URI" to @value in application.properties
        HttpHeaders headers = new HttpHeaders();
        MediaType mediaType = new MediaType("application", "json", Charset.forName("UTF-8"));
        headers.set("Authorization", "Bearer "+token);
        headers.setContentType(mediaType);

        HttpEntity<String> entity = new HttpEntity<String>(headers);

        String id = dto.getChargeStationId();

        ResponseEntity<String> response = null;
        try {
            response = restTemplate.exchange(new URI(
                            urlChargestations + "/" + id),
                    HttpMethod.GET,
                    entity,
                    String.class);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        if (response.getStatusCode() == HttpStatus.OK) {
            responsebody = response.getBody();
        } else {
            throw new CustomHTTPGenericException(HttpStatus.INTERNAL_SERVER_ERROR, Constants.MESSAGE_CODE_ERRORS.REDEEM_ERROR);
        }
        return responsebody;

    }

    public String edrvUser() {

        RestTemplate restTemplate = new RestTemplate();
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
        CloseableHttpClient httpClient = HttpClientBuilder.create()
                .setRedirectStrategy(new LaxRedirectStrategy())
                .build();
        factory.setHttpClient(httpClient);
        restTemplate.setRequestFactory(factory);

        String responsebody = null;

        // TODO: 28/1/2021 AD change value from hard code "x-api-key" and "URI" to @value in application.properties
        HttpHeaders headers = new HttpHeaders();
        MediaType mediaType = new MediaType("application", "json", Charset.forName("UTF-8"));
        headers.set("Authorization", "Bearer "+token);
        headers.setContentType(mediaType);

        HttpEntity<String> entity = new HttpEntity<String>(headers);

        ResponseEntity<String> response = null;
        try {
            response = restTemplate.exchange(new URI(
                            urlUser),
                    HttpMethod.GET,
                    entity,
                    String.class);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        if (response.getStatusCode() == HttpStatus.OK) {
            responsebody = response.getBody();
        } else {
            throw new CustomHTTPGenericException(HttpStatus.INTERNAL_SERVER_ERROR, Constants.MESSAGE_CODE_ERRORS.REDEEM_ERROR);
        }
        return responsebody;

    }
}
