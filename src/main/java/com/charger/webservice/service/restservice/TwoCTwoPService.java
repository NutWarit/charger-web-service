package com.charger.webservice.service.restservice;

import com.charger.webservice.DTO.twoCtwoP.PaymentTokenDTO;
import com.charger.webservice.exceptions.CustomHTTPGenericException;
import com.charger.webservice.util.Constants;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.List;

@Service
public class TwoCTwoPService {

    @Value("${2c2p.api.url.paymentToken}")
    private String urlPaymentToken;
    @Value("${2c2p.api.url.cardToken}")
    private String urlCardToken;

    public String paymentToken(String token) {

        RestTemplate restTemplate = new RestTemplate();
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
        CloseableHttpClient httpClient = HttpClientBuilder.create()
                .setRedirectStrategy(new LaxRedirectStrategy())
                .build();
        factory.setHttpClient(httpClient);
        restTemplate.setRequestFactory(factory);

        String responsebody = null;

        // TODO: 28/1/2021 AD change value from hard code "x-api-key" and "URI" to @value in application.properties
        HttpHeaders headers = new HttpHeaders();
        List<MediaType> accept = Collections.singletonList(MediaType.TEXT_PLAIN);
        MediaType mediaType = new MediaType("application", "json", Charset.forName("UTF-8"));
        headers.setAccept(accept);
        headers.setContentType(mediaType);

        PaymentTokenDTO dto = new PaymentTokenDTO();
        dto.setPayload(token);

        ObjectMapper objectMapper = new ObjectMapper();
        String value = null;
        try {
            value = objectMapper.writeValueAsString(dto);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        HttpEntity<String> entity = new HttpEntity<String>(value, headers);

        ResponseEntity<String> response = null;
        try {
            response = restTemplate.exchange(new URI(
                            urlPaymentToken),
                    HttpMethod.POST,
                    entity,
                    String.class);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        if (response.getStatusCode() == HttpStatus.OK) {
            responsebody = response.getBody();
        } else {
            throw new CustomHTTPGenericException(HttpStatus.INTERNAL_SERVER_ERROR, Constants.MESSAGE_CODE_ERRORS.REDEEM_ERROR);
        }
        return responsebody;

    }

}
