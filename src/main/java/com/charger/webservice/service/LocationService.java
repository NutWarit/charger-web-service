package com.charger.webservice.service;

import com.charger.webservice.DTO.LocationDTO;
import com.charger.webservice.entity.Location;
import com.charger.webservice.entity.user.User;
import com.charger.webservice.exceptions.BadRequestException;
import com.charger.webservice.exceptions.CustomHTTPGenericException;
import com.charger.webservice.exceptions.UnauthorizedException;
import com.charger.webservice.repository.LocationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Service
public class LocationService {

    private static final DecimalFormat df2 = new DecimalFormat("#,###,###,##0.00");

    @Autowired
    private LocationRepository locationRepository;

    @PersistenceContext
    private EntityManager entityManager;

    public PageImpl<Location> list(Integer page, Integer size, Authentication authentication) {

        User user = (User) authentication.getPrincipal();
        if (user == null) {
            throw new CustomHTTPGenericException(HttpStatus.UNAUTHORIZED, "Not Authorized");
        }

        StringBuffer strSelectQuery = new StringBuffer("select l from Location l where 1=1");
        StringBuffer strCountQuery = new StringBuffer("SELECT count(l.id) FROM Location l where 1=1");

        Query query = entityManager.createQuery(strSelectQuery.toString());
        query.setFirstResult((page - 1) * size);
        query.setMaxResults(size);
        Query queryCount = entityManager.createQuery(strCountQuery.toString());

        long countResults = (long) queryCount.getSingleResult();

        List<Location> locationList = query.getResultList();

        PageImpl<Location> pageResult = new PageImpl<Location>(locationList, PageRequest.of(page - 1, size), countResults);

        return pageResult;
    }

    public LocationDTO get(Integer locationId,
                           Authentication authentication
    ) {
        Location location = locationRepository.findOneById(locationId);
        if (location == null) {
            throw new BadRequestException(HttpStatus.BAD_REQUEST, "Not found Id");
        }

        User user = (User) authentication.getPrincipal();
        if (user == null) {
            throw new UnauthorizedException(HttpStatus.UNAUTHORIZED, "Not Authorized");
        }

        LocationDTO dto = new LocationDTO();
        dto.setId(location.getId());
        dto.setStreet(location.getStreet());
        dto.setSubDistrict(location.getSubDistrict());
        dto.setDistrict(location.getDistrict());
        dto.setProvince(location.getProvince());
        dto.setPostcode(location.getPostcode());
        dto.setCountry(location.getCountry());
        dto.setLocationType(location.getLocationType());
        dto.setLocationPosition(location.getLocationPosition());

        return dto;

    }

    public Location create(LocationDTO dto,Authentication authentication) {

        User user = (User) authentication.getPrincipal();
        if (user == null) {
            throw new UnauthorizedException(HttpStatus.UNAUTHORIZED, "Not Authorized");
        }

        Location location = new Location();
        location.setStreet(dto.getStreet());
        location.setSubDistrict(dto.getSubDistrict());
        location.setDistrict(dto.getDistrict());
        location.setProvince(dto.getProvince());
        location.setPostcode(dto.getPostcode());
        location.setCountry(dto.getCountry());
        location.setLocationType(dto.getLocationType());
        location.setLocationPosition(dto.getLocationPosition());

        location.setCreatedBy(user.getUsername());
        location.setCreatedDate(new Date());
        location.setUpdatedBy(user.getUsername());
        location.setUpdatedDate(new Date());

        locationRepository.save(location);

        return location;

    }

    public Location update(LocationDTO dto,Authentication authentication) {

        User user = (User) authentication.getPrincipal();
        if (user == null) {
            throw new UnauthorizedException(HttpStatus.UNAUTHORIZED, "Not Authorized");
        }

        Location location = locationRepository.findOneById(dto.getId());

        location.setStreet(dto.getStreet());
        location.setSubDistrict(dto.getSubDistrict());
        location.setDistrict(dto.getDistrict());
        location.setProvince(dto.getProvince());
        location.setPostcode(dto.getPostcode());
        location.setCountry(dto.getCountry());
        location.setLocationType(dto.getLocationType());
        location.setLocationPosition(dto.getLocationPosition());

        location.setUpdatedBy(user.getUsername());
        location.setUpdatedDate(new Date());

        locationRepository.save(location);

        return location;

    }

    public void delete(
            Integer connectorId,
            Authentication authentication
    ){
        User user = (User) authentication.getPrincipal();

        if (user == null) {
            throw new BadRequestException(HttpStatus.UNAUTHORIZED, "User is UNAUTHORIZED");
        }
        locationRepository.deleteById(connectorId);
    }
}
