package com.charger.webservice.service;

import com.charger.webservice.DTO.UserDTO;
import com.charger.webservice.entity.VerifyToken;
import com.charger.webservice.entity.user.User;
import com.charger.webservice.exceptions.BadRequestException;
import com.charger.webservice.exceptions.UnauthorizedException;
import com.charger.webservice.repository.UserRepository;
import com.charger.webservice.repository.VerifyTokenRepository;
import com.charger.webservice.wrapper.UserWrapper;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.DelegatingPasswordEncoder;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class UserService {
    @Value("${url.register}")
    private String urlRegister;

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private EmailService emailService;
    @Autowired
    private VerifyTokenRepository verifyTokenRepository;

    @Autowired
    private DelegatingPasswordEncoder delegatingPasswordEncoder;
    @Autowired
    private DefaultTokenServices tokenServices;

    public static org.slf4j.Logger logger = LoggerFactory.getLogger(UserService.class);

    public static boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public UserWrapper get(Authentication authentication) {
        User authUser = (User) authentication.getPrincipal();
        if (authUser == null) {
            throw new UnauthorizedException(HttpStatus.UNAUTHORIZED, "Not Authorized");
        }
        UserWrapper wrapper = new UserWrapper();
        User u = userRepository.getById(authUser.getId());
        wrapper.setId(u.getId());
        wrapper.setEmail(u.getUsername());
        wrapper.setProfileImageUrl(u.getProfileImageUrl());
        wrapper.setFirstName(u.getFirstName());
        wrapper.setLastName(u.getLastName());
        wrapper.setCardNumber(u.getCardNumber());
        wrapper.setPhoneNumber(u.getPhoneNumber());
        wrapper.setGender(u.getGender());
        wrapper.setAge(u.getAge());
        wrapper.setKyc(u.isKyc());
        wrapper.setOtp(u.isOtp());
        wrapper.setPin(u.getPin());
        return wrapper;
    }

    public User createUser(UserDTO dto) {

        User u = null;
        if (dto.getId() != null) {
            u = userRepository.findOneById(dto.getId());
            if (u == null) {
                throw new BadRequestException(HttpStatus.BAD_REQUEST, "Invalid user Id.");
            }
        } else {
            u = new User();
            User check = userRepository.findByEmail(dto.getEmail());
            if (check != null) {
                throw new BadRequestException(HttpStatus.BAD_REQUEST, "Email is already exist.");
            }
        }

        if (!isEmailValid(dto.getEmail())) {
            throw new BadRequestException(HttpStatus.BAD_REQUEST, "Email is invalid");
        }

        validatePassword(dto.getPassword());

        try {
            u.setUsername(dto.getUsername());
            u.setEmail(dto.getEmail());
            u.setPassword(delegatingPasswordEncoder.encode(dto.getPassword()));
            u.setProfileImageUrl(dto.getProfileImageUrl());
            u.setKyc(dto.getId() != null && dto.isKyc());
            u.setOtp(dto.getId() != null && dto.isOpt());
            u.setAccept(dto.getId() != null && dto.isAccept());
            u.setEnabled(true);
            u.setNonLocked(true);
            u.setVerifyEmail(false);
            u.setCreatedDate(new Date());
            u.setCreatedBy(dto.getUsername());
            u.setUpdatedDate(new Date());
            u.setUpdatedBy(dto.getUsername());
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new BadRequestException(HttpStatus.INTERNAL_SERVER_ERROR,"Something went wrong sever error");
        }

        userRepository.saveAndFlush(u);

        //Send Email to Register Confirm
        VerifyToken vt = new VerifyToken();
        vt.setUserId(u.getId());
        vt.setType("REGISTER");
        vt.setToken(UUID.randomUUID().toString());
        vt.setCreatedDate(new Date());
        verifyTokenRepository.save(vt);

        String fullName = dto.getEmail();
        Map<String, Object> data = new TreeMap<>();
        data.put("username", fullName);
        data.put("token", vt.getToken());
        data.put("urlRegister", urlRegister);
        List<String> emails = new ArrayList<>();
        emails.add(dto.getEmail());
        emailService.sendEmail("confirm_registered.html", "แจ้งผลการสมัครสมาชิก", data, emails, null, null);

        return u;

    }

    public User updateUser(UserDTO userDTO) {

        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        List<User> userCheck = userRepository.findByEmails(userDTO.getUsername());

        if (userCheck != null && userCheck.size() > 0 && !userCheck.get(0).getId().equals(userDTO.getId())) {
            throw new RuntimeException("Username Not Empty");
        }

        User userEdit = this.userRepository.findOneById(userDTO.getId());
        userEdit.setUsername(userDTO.getUsername());
        userEdit.setEmail(userDTO.getEmail());
        userEdit.setPassword(delegatingPasswordEncoder.encode(userDTO.getPassword()));
        userEdit.setProfileImageUrl(userDTO.getProfileImageUrl());
        userEdit.setFirstName(userDTO.getFirstName());
        userEdit.setLastName(userDTO.getLastName());
        userEdit.setCardNumber(userDTO.getCardNumber());
        userEdit.setPhoneNumber(userDTO.getPhoneNumber());
        userEdit.setGender(userDTO.getGender());
        userEdit.setAge(userDTO.getAge());

        this.userRepository.save(userEdit);

        return userEdit;

    }

    public void logout(HttpServletRequest request) {
        String authHeader = request.getHeader("Authorization");
        if (authHeader != null) {
            String tokenValue = authHeader.replace("Bearer", "").trim();
            tokenServices.revokeToken(tokenValue);
        }
    }

    public User verifyRegisterToken(
            String token
    ){
        VerifyToken verifyToken = verifyTokenRepository.findOneByTokenAndType(token, "REGISTER");
        if (verifyToken == null){
            throw new BadRequestException(HttpStatus.BAD_REQUEST, "Invalid Verify Token.");
        }
        User user = userRepository.findOneById(verifyToken.getUserId());
        user.setVerifyEmail(true);
        userRepository.saveAndFlush(user);
        verifyTokenRepository.delete(verifyToken);
        return user;
    }

    private void validatePassword(
            String password
    ){
        if ((password.length() < 8 || password.length() > 32)) {
            throw new BadRequestException(HttpStatus.BAD_REQUEST, "Password format invalid.");
        }
        if (!(password.matches(".*[a-z].*") && password.matches(".*[0-9].*") && password.matches(".*[A-Z].*"))){
            throw new BadRequestException(HttpStatus.BAD_REQUEST, "Password format invalid");
        }
    }

}
