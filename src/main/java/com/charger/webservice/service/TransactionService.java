package com.charger.webservice.service;

import com.charger.webservice.DTO.*;
import com.charger.webservice.DTO.edrv.ResultEnergyReport;
import com.charger.webservice.DTO.edrv.RootEnergyReport;
import com.charger.webservice.entity.*;
import com.charger.webservice.entity.user.User;
import com.charger.webservice.exceptions.BadRequestException;
import com.charger.webservice.exceptions.CustomHTTPGenericException;
import com.charger.webservice.exceptions.UnauthorizedException;
import com.charger.webservice.repository.ChargerRepository;
import com.charger.webservice.repository.ChargerTransactionRepository;
import com.charger.webservice.repository.ConnectorRepository;
import com.charger.webservice.repository.PaymentTransactionRepository;
import com.charger.webservice.wrapper.ChargerTransactionWrapper;
import com.charger.webservice.wrapper.PaymentWrapper;
import com.charger.webservice.wrapper.TransactionWrapper;
import com.google.zxing.*;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Service
public class TransactionService {

    private static final DecimalFormat df2 = new DecimalFormat("#,###,###,##0.00");

    @Autowired
    ConnectorRepository connectorRepository;
    @Autowired
    ChargerTransactionRepository chargerTransactionRepository;

    @Value("${file.outputPath}")
    private String fileOutputPath;
    @Value("${folder.QRCode}")
    private String ORCode;

    public TransactionWrapper get(Integer transactionId,
                                  Authentication authentication
    ) {
        ChargerTransaction chargerTransaction = chargerTransactionRepository.findOneById(transactionId);
        if (transactionId == null) {
            throw new BadRequestException(HttpStatus.BAD_REQUEST, "Not found Id");
        }

        User user = (User) authentication.getPrincipal();
        if (user == null) {
            throw new UnauthorizedException(HttpStatus.UNAUTHORIZED, "Not Authorized");
        }

        TransactionWrapper wrapper = new TransactionWrapper();
        wrapper.setId(chargerTransaction.getId());
        wrapper.setConnectorId(chargerTransaction.getConnectorId());
        wrapper.setUserId(chargerTransaction.getUserId());
        wrapper.setStartTime(chargerTransaction.getStartTime());
        wrapper.setStopTime(chargerTransaction.getStopTime());
        wrapper.setChargerTransactionStatus(chargerTransaction.getChargerTransactionStatus());
        wrapper.setSessionId(chargerTransaction.getSessionId());

        return wrapper;

    }

    public ChargerTransactionWrapper scanQRTransaction(ORCodeDTO dto, Authentication authentication) throws IOException {

        User user = (User) authentication.getPrincipal();
        if (user == null) {
            throw new CustomHTTPGenericException(HttpStatus.UNAUTHORIZED, "Not Authorized");
        }

        String QRToken = readORCode(dto.getFileName());

        Connector connector = connectorRepository.findOneByQrCodeToken(QRToken);

        ChargerTransaction chargerTransaction = new ChargerTransaction();

        chargerTransaction.setConnectorId(connector.getId());
        chargerTransaction.setUserId(user.getId());
        chargerTransaction.setChargerTransactionStatus("WAITING_PLUGIN");

        chargerTransaction.setCreatedBy(user.getUsername());
        chargerTransaction.setCreatedDate(new Date());
        chargerTransaction.setUpdatedBy(user.getUsername());
        chargerTransaction.setUpdatedDate(new Date());

        chargerTransactionRepository.saveAndFlush(chargerTransaction);

        ChargerTransactionWrapper wrapper = new ChargerTransactionWrapper();
        wrapper.setChargerTransactionId(chargerTransaction.getId());
        wrapper.setChargerTransactionStatus(chargerTransaction.getChargerTransactionStatus());
        wrapper.setQrToken(QRToken);

        return wrapper;
    }

    public String readORCode(String fileName) throws IOException {

        String filePath = fileOutputPath + "/" + ORCode + "/" + fileName + ".png";

        BinaryBitmap binaryBitmap
                = new BinaryBitmap(new HybridBinarizer(
                new BufferedImageLuminanceSource(
                        ImageIO.read(
                                new FileInputStream(filePath)))));

        Result result = null;
        try {
            result = new MultiFormatReader().decode(binaryBitmap);
        } catch (NotFoundException e) {
            e.printStackTrace();
        }

        return result.getText();
    }

    public ChargerTransaction chargerStartTransaction(Integer chargerTransactionId, String sessionId, Authentication authentication) {

        User user = (User) authentication.getPrincipal();
        if (user == null) {
            throw new CustomHTTPGenericException(HttpStatus.UNAUTHORIZED, "Not Authorized");
        }

        ChargerTransaction chargerTransaction =
                chargerTransactionRepository.findOneById(chargerTransactionId);

        chargerTransaction.setStartTime(new Date());
        chargerTransaction.setChargerTransactionStatus("CHARGING");
        chargerTransaction.setSessionId(sessionId);

        chargerTransactionRepository.save(chargerTransaction);

        return chargerTransaction;

    }

    public String chargerStopTransaction(Integer chargerTransactionId, Authentication authentication) {

        User user = (User) authentication.getPrincipal();
        if (user == null) {
            throw new CustomHTTPGenericException(HttpStatus.UNAUTHORIZED, "Not Authorized");
        }

        ChargerTransaction ct = chargerTransactionRepository.findOneById(chargerTransactionId);

        ct.setStopTime(new Date());
        ct.setChargerTransactionStatus("STOP_CHARGING");

        chargerTransactionRepository.saveAndFlush(ct);

        return "stop charging";

    }

}
