package com.charger.webservice.service;

import com.charger.webservice.DTO.ConnectorDTO;
import com.charger.webservice.DTO.edrv.*;
import com.charger.webservice.entity.Charger;
import com.charger.webservice.entity.Connector;
import com.charger.webservice.entity.user.User;
import com.charger.webservice.exceptions.BadRequestException;
import com.charger.webservice.exceptions.CustomHTTPGenericException;
import com.charger.webservice.exceptions.UnauthorizedException;
import com.charger.webservice.repository.ChargerRepository;
import com.charger.webservice.repository.ConnectorRepository;
import com.google.zxing.*;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.DelegatingPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.File;
import java.io.IOException;
import java.util.*;

@Service
public class ConnectorService {

    @Autowired
    private ConnectorRepository connectorRepository;
    @Autowired
    private ChargerRepository chargerRepository;

    @Value("${file.outputPath}")
    private String fileOutputPath;

    @PersistenceContext
    private EntityManager entityManager;

    public PageImpl<Connector> list(Integer page, Integer size, Authentication authentication) {

        User user = (User) authentication.getPrincipal();
        if (user == null) {
            throw new CustomHTTPGenericException(HttpStatus.UNAUTHORIZED, "Not Authorized");
        }

        StringBuffer strSelectQuery = new StringBuffer("select cn from Connector cn where 1=1");
        StringBuffer strCountQuery = new StringBuffer("SELECT count(cn.id) FROM Connector cn where 1=1");

        Query query = entityManager.createQuery(strSelectQuery.toString());
        query.setFirstResult((page - 1) * size);
        query.setMaxResults(size);
        Query queryCount = entityManager.createQuery(strCountQuery.toString());

        long countResults = (long) queryCount.getSingleResult();

        List<Connector> connectorList = query.getResultList();

        PageImpl<Connector> pageResult = new PageImpl<Connector>(connectorList, PageRequest.of(page - 1, size), countResults);

        return pageResult;
    }

    public ConnectorDTO get(Integer chargerId,
                            Authentication authentication
    ) {
        Connector connector = connectorRepository.findOneById(chargerId);
        if (connector == null) {
            throw new BadRequestException(HttpStatus.BAD_REQUEST, "Not found Id");
        }

        User user = (User) authentication.getPrincipal();
        if (user == null) {
            throw new UnauthorizedException(HttpStatus.UNAUTHORIZED, "Not Authorized");
        }

        ConnectorDTO dto = new ConnectorDTO();
        dto.setId(connector.getId());
        dto.setChargerId(connector.getChargerId());
        dto.setConnectorType(connector.getConnectorType());
        dto.setConnectorNo(connector.getConnectorNo());
        dto.setConnectorRateKw(connector.getConnectorRateKw());
        dto.setConnectorRatePerMin(connector.getConnectorRatePerMin());
        dto.setConnectorRatePrice(connector.getConnectorRatePrice());
        dto.setConnectorRateHours(connector.getConnectorRateHours());
        dto.setConnectorRateStart(connector.getConnectorRateStart());
        dto.setQrCodeToken(connector.getQrCodeToken());
        dto.setQrCodeImageUrl(connector.getQrCodeImageUrl());

        return dto;

    }

    public Connector create(ConnectorDTO dto, RootChargeStations rootChargeStations, RootUser rootUser, Authentication authentication) throws IOException, WriterException {

        User user = (User) authentication.getPrincipal();
        if (user == null) {
            throw new UnauthorizedException(HttpStatus.UNAUTHORIZED, "Not Authorized");
        }

        List<Map<String, Object>> maps = new ArrayList<>();

        // connector token
        for (ConnectorChargeStasions cc : rootChargeStations.getResult().getConnectors()) {

            Map<String, Object> map = new HashMap<>();
            map.put("id", cc.get_id());
            map.put("type", cc.getPower_type());

            maps.add(map);
        }

        Connector connector = new Connector();

        for (int i = 0; i < maps.size(); i++) {

            connector.setChargerId(dto.getChargerId());
            connector.setConnectorType((String) maps.get(i).get("type"));
            connector.setConnectorNo(i+1);
            connector.setUser_token(rootUser.getResult().get(0).get_id());
            connector.setConnector_token((String) maps.get(i).get("id"));

            Charger charger = chargerRepository.findOneById(connector.getChargerId());

            // The data that the QR code will contain
            String dataQRCode = charger.getName() + "_" + connector.getConnectorNo();
            String token = charger.getName() + " No." + connector.getConnectorNo();

            String path = generateQR(dataQRCode,token);

            connector.setQrCodeToken(token);

            connector.setQrCodeImageUrl(path);

            connector.setCreatedBy(user.getUsername());
            connector.setCreatedDate(new Date());
            connector.setUpdatedBy(user.getUsername());
            connector.setUpdatedDate(new Date());

            connectorRepository.save(connector);
        }

        return connector;

    }

    public Connector update(ConnectorDTO dto,Authentication authentication) {

        User user = (User) authentication.getPrincipal();
        if (user == null) {
            throw new UnauthorizedException(HttpStatus.UNAUTHORIZED, "Not Authorized");
        }

        Connector connector = connectorRepository.findOneById(dto.getId());

        connector.setConnectorRateKw(dto.getConnectorRateKw());
        connector.setConnectorRatePerMin(dto.getConnectorRatePerMin());
        connector.setConnectorRatePrice(dto.getConnectorRatePrice());
        connector.setConnectorRateHours(dto.getConnectorRateHours());
        connector.setConnectorRateStart(dto.getConnectorRateStart());

        connector.setUpdatedBy(user.getUsername());
        connector.setUpdatedDate(new Date());

        connectorRepository.save(connector);

        return connector;

    }

    public void delete(
            Integer connectorId,
            Authentication authentication
    ){
        User user = (User) authentication.getPrincipal();

        if (user == null) {
            throw new BadRequestException(HttpStatus.UNAUTHORIZED, "User is UNAUTHORIZED");
        }
        connectorRepository.deleteById(connectorId);
    }

    public String generateQR(String data,String token)
            throws WriterException, IOException
    {

        String folder = "QRCode";
        genFolder(folder);

        String fileName = data + ".png";

        // The path where the image will get saved
        String pathFileName = fileOutputPath +"/"+ folder +"/"+ fileName;

        Map<EncodeHintType, ErrorCorrectionLevel> hashMap
                = new HashMap<EncodeHintType,
                                ErrorCorrectionLevel>();

        hashMap.put(EncodeHintType.ERROR_CORRECTION,
                ErrorCorrectionLevel.L);

        BitMatrix matrix = new MultiFormatWriter().encode(
                new String(token.getBytes("UTF-8"), "UTF-8"),
                BarcodeFormat.QR_CODE, 200, 200);

        MatrixToImageWriter.writeToFile(
                matrix,
                pathFileName.substring(pathFileName.lastIndexOf('.') + 1),
                new File(pathFileName));

        return pathFileName;

    }

    private void genFolder(
            String folder
    ){
        if (folder != null && !folder.isEmpty()) {
            File directoryToUpload = new File(this.fileOutputPath + File.separator + folder);
            if (!directoryToUpload.exists()) {
                directoryToUpload.mkdir();
            }
        }
    }

}
