package com.charger.webservice.service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.charger.webservice.DTO.BillPageDTO;
import com.charger.webservice.DTO.PaymentTransactionDTO;
import com.charger.webservice.DTO.edrv.ResultEnergyReport;
import com.charger.webservice.DTO.edrv.RootEnergyReport;
import com.charger.webservice.DTO.twoCtwoP.RootPaymentToken;
import com.charger.webservice.entity.ChargerTransaction;
import com.charger.webservice.entity.Connector;
import com.charger.webservice.entity.PaymentTransaction;
import com.charger.webservice.entity.user.User;
import com.charger.webservice.exceptions.BadRequestException;
import com.charger.webservice.exceptions.CustomHTTPGenericException;
import com.charger.webservice.exceptions.UnauthorizedException;
import com.charger.webservice.repository.ChargerTransactionRepository;
import com.charger.webservice.repository.ConnectorRepository;
import com.charger.webservice.repository.PaymentTransactionRepository;
import com.charger.webservice.repository.UserRepository;
import com.charger.webservice.service.restservice.TwoCTwoPService;
import com.charger.webservice.wrapper.PaymentWrapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Service
public class PaymentService {

    private static final DecimalFormat df = new DecimalFormat("0.00");

    @Value("${2c2p.secretKey}")
    private String secretKey;
    @Value("${2c2p.merchantId}")
    private String merchantId;

    @Autowired
    ConnectorRepository connectorRepository;
    @Autowired
    ChargerTransactionRepository chargerTransactionRepository;
    @Autowired
    PaymentTransactionRepository paymentTransactionRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    TwoCTwoPService twoCTwoPService;

    public PaymentWrapper getPayment(Integer paymentTransactionId,
                                     Authentication authentication
    ) {
        PaymentTransaction paymentTransaction = paymentTransactionRepository.findOneById(paymentTransactionId);
        if (paymentTransactionId == null) {
            throw new BadRequestException(HttpStatus.BAD_REQUEST, "Not found Id");
        }

        User user = (User) authentication.getPrincipal();
        if (user == null) {
            throw new UnauthorizedException(HttpStatus.UNAUTHORIZED, "Not Authorized");
        }

        PaymentWrapper wrapper = new PaymentWrapper();
        wrapper.setId(paymentTransaction.getId());
        wrapper.setChargerTransactionId(paymentTransaction.getChargerTransactionId());
        wrapper.setTotalRateKw(paymentTransaction.getTotalRateKw());
        wrapper.setTotalRatePerMin(paymentTransaction.getTotalRatePerMin());
        wrapper.setTotalTime(paymentTransaction.getTotalTime());
        wrapper.setRateCostKwh(paymentTransaction.getRateCostKwh());
        wrapper.setRateCostTime(paymentTransaction.getRateCostTime());
        wrapper.setRateCostStart(paymentTransaction.getRateCostStart());
        wrapper.setTotalCost(paymentTransaction.getTotalCost());
        wrapper.setPaymentTransactionType(paymentTransaction.getPaymentTransactionType());
        wrapper.setPaymentStatus(paymentTransaction.getPaymentStatus());

        return wrapper;

    }

    public PaymentTransaction paymentTransaction(PaymentTransactionDTO dto, Authentication authentication) {

        User user = (User) authentication.getPrincipal();
        if (user == null) {
            throw new CustomHTTPGenericException(HttpStatus.UNAUTHORIZED, "Not Authorized");
        }

        PaymentTransaction paymentTransaction = new PaymentTransaction();
        paymentTransaction.setChargerTransactionId(dto.getChargerTransactionId());
        paymentTransaction.setPaymentTransactionType(dto.getPaymentTransactionType());
        paymentTransaction.setTotalCost(dto.getTotalCost());
        paymentTransaction.setPaymentStatus("WAITING_PAYMENT");

        paymentTransaction.setCreatedBy(user.getUsername());
        paymentTransaction.setCreatedDate(new Date());
        paymentTransaction.setUpdatedBy(user.getUsername());
        paymentTransaction.setUpdatedDate(new Date());

        paymentTransactionRepository.save(paymentTransaction);

        return paymentTransaction;
    }

    public PaymentTransaction calculateCost(Integer chargerTransactionId, RootEnergyReport responseEnergy, Authentication authentication) {

        User user = (User) authentication.getPrincipal();
        if (user == null) {
            throw new CustomHTTPGenericException(HttpStatus.UNAUTHORIZED, "Not Authorized");
        }

        ChargerTransaction ct = chargerTransactionRepository.findOneById(chargerTransactionId);

        PaymentTransaction pt = paymentTransactionRepository.findOneById(ct.getId());
        Connector connector = connectorRepository.findOneById(ct.getConnectorId());

        List<String> energyMeterList = new ArrayList<>();
        List<String> unit = new ArrayList<>();

        // เอาค่า EnergyMeter หน่วย Kwh ออกมา
        for(ResultEnergyReport resultEnergyReport : responseEnergy.getResult()) {

            energyMeterList.add(resultEnergyReport.getEnergy_meter().getValue());

            unit.add(resultEnergyReport.getEnergy_meter().getUnit());

        }

        long time = ct.getStopTime().getTime() - ct.getStartTime().getTime();

        long hours = TimeUnit.MILLISECONDS.toHours(time);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(time);
        long calculateMinutes = Long.parseLong(df.format(minutes/60));

        Double totalTime = null;
        Double totalRateKwh = null;

        if(minutes < 10) {
            String s = "0" + minutes;
            totalTime = Double.valueOf(hours + "." + s);
        } else {
            totalTime = Double.valueOf(hours + "." + minutes);
        }

        if(unit.get(0).equals("kWh")) {
            totalRateKwh = Double.valueOf(Double.parseDouble(energyMeterList.get(0)) - Double.parseDouble(energyMeterList.get(1)));
        } else {
            totalRateKwh = Double.valueOf(Double.parseDouble(energyMeterList.get(0)) - Double.parseDouble(energyMeterList.get(1))) / 1000;
        }

        Double totalRateKw = Double.valueOf(totalRateKwh / (hours + calculateMinutes));
        Double rateCostKwh = Double.valueOf(totalRateKwh * connector.getConnectorRatePrice());
        Double rateCostTime = Double.valueOf((hours * connector.getConnectorRateHours()) + (calculateMinutes * connector.getConnectorRateHours()));
        Double rateCostStart = connector.getConnectorRateStart();
        Double totalRatePerMin = null;
        if (hours != 0) {
            totalRatePerMin = Double.valueOf(totalRateKw / ((hours * 60) + minutes));
        } else {
            totalRatePerMin = Double.valueOf(totalRateKw / minutes);
        }
        Double totalCost = rateCostKwh + rateCostTime + rateCostStart;

        pt.setInvoiceNo(String.format("%010d", pt.getId()));
        pt.setTotalRateKw(totalRateKw);
        pt.setTotalRatePerMin(totalRatePerMin);
        pt.setTotalRateKwh(totalRateKwh);
        pt.setRateCostKwh(rateCostKwh);
        pt.setRateCostTime(rateCostTime);
        pt.setRateCostStart(rateCostStart);
        pt.setTotalCost(totalCost);
        pt.setTotalTime(totalTime);

        paymentTransactionRepository.save(pt);

        return pt;

    }

    public BillPageDTO billPayment(Integer paymentTransactionId, Authentication authentication) throws JsonProcessingException {

        User user = (User) authentication.getPrincipal();
        if (user == null) {
            throw new CustomHTTPGenericException(HttpStatus.UNAUTHORIZED, "Not Authorized");
        }

        User u = userRepository.findOneById(user.getId());
        PaymentTransaction paymentTransaction = paymentTransactionRepository.findOneById(paymentTransactionId);

        String token = jwtGenerate(paymentTransaction,u);

        String response = twoCTwoPService.paymentToken(token);
        ObjectMapper mapper = new ObjectMapper();
        RootPaymentToken responsePaymentToken = mapper.readValue(response, RootPaymentToken.class);

        DecodedJWT jwt = JWT.decode(responsePaymentToken.getPayload());
        String url = jwt.getClaim("webPaymentUrl").asString();

        BillPageDTO dto = new BillPageDTO();
        dto.setWebPaymentUrl(url);

        return dto;
    }

    public String jwtGenerate(PaymentTransaction paymentTransaction,User user) {

        String[] arr = {"CC"};

        String token="";

        HashMap<String, Object> payload = new HashMap<>();
        payload.put("merchantID",merchantId);
        payload.put("invoiceNo",paymentTransaction.getInvoiceNo());
        payload.put("description","item 1");
        payload.put("amount",paymentTransaction.getTotalCost());
        payload.put("currencyCode","THB");
        payload.put("paymentChannel",arr);
        payload.put("tokenize","True");
        if(user.getCardToken() != null) {
            String[] card = {user.getCardToken()};
            payload.put("cardTokens",card);
        }

        try {
            Algorithm algorithm = Algorithm.HMAC256(secretKey);

            token = JWT.create()
                    .withPayload(payload).sign(algorithm);

        } catch (JWTCreationException | IllegalArgumentException e){
            //Invalid Signing configuration / Couldn't convert Claims.
            e.printStackTrace();
        }

        return token;
    }

}
