package com.charger.webservice.service;

import com.charger.webservice.DTO.ChargerDTO;
import com.charger.webservice.entity.Charger;
import com.charger.webservice.entity.user.User;
import com.charger.webservice.exceptions.BadRequestException;
import com.charger.webservice.exceptions.CustomHTTPGenericException;
import com.charger.webservice.exceptions.UnauthorizedException;
import com.charger.webservice.repository.ChargerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.*;

@Service
public class ChargerService {

    @Autowired
    private ChargerRepository chargerRepository;

    @PersistenceContext
    private EntityManager entityManager;

    public PageImpl<Charger> list(String name, String status, Integer page, Integer size, Authentication authentication) {

        User user = (User) authentication.getPrincipal();
        if (user == null) {
            throw new CustomHTTPGenericException(HttpStatus.UNAUTHORIZED, "Not Authorized");
        }

        StringBuffer strSelectQuery = new StringBuffer("select c from Charger c where 1=1");
        StringBuffer strCountQuery = new StringBuffer("SELECT count(c.id) FROM Charger c where 1=1");

        if (name != null && !name.isEmpty()) {
            strSelectQuery.append(" and c.name = :name");
            strCountQuery.append(" and c.name = :name");
        }

        if (status != null && !status.isEmpty()) {
            strSelectQuery.append(" and c.status = :status");
            strCountQuery.append(" and c.status = :status");
        }

        Query query = entityManager.createQuery(strSelectQuery.toString());
        query.setFirstResult((page - 1) * size);
        query.setMaxResults(size);
        Query queryCount = entityManager.createQuery(strCountQuery.toString());

        if (name != null && !name.isEmpty()) {
            query.setParameter("name",name);
            queryCount.setParameter("name",name);
        }

        if (status != null && !status.isEmpty()) {
            query.setParameter("status",status);
            queryCount.setParameter("status",status);
        }

        long countResults = (long) queryCount.getSingleResult();

        List<Charger> chargerList = query.getResultList();

        PageImpl<Charger> pageResult = new PageImpl<Charger>(chargerList, PageRequest.of(page - 1, size), countResults);

        return pageResult;
    }

    public ChargerDTO get(Integer chargerId,
                          Authentication authentication
    ) {
        Charger charger = chargerRepository.findOneById(chargerId);
        if (charger == null) {
            throw new BadRequestException(HttpStatus.BAD_REQUEST, "Not found Id");
        }

        User user = (User) authentication.getPrincipal();
        if (user == null) {
            throw new UnauthorizedException(HttpStatus.UNAUTHORIZED, "Not Authorized");
        }

        ChargerDTO dto = new ChargerDTO();
        dto.setId(charger.getId());
        dto.setChargerStatusId(charger.getChargerStatusId());
        dto.setLocationId(charger.getLocationId());
        dto.setChargerImageUrl(charger.getChargerImageUrl());
        dto.setName(charger.getName());
        dto.setStatus(charger.getStatus());

        return dto;

    }

    public Charger create(ChargerDTO dto,Authentication authentication) {

        User user = (User) authentication.getPrincipal();
        if (user == null) {
            throw new UnauthorizedException(HttpStatus.UNAUTHORIZED, "Not Authorized");
        }

        Charger charger = new Charger();
        charger.setChargerStatusId(dto.getChargerStatusId());
        charger.setLocationId(dto.getLocationId());
        charger.setChargerImageUrl(dto.getChargerImageUrl());
        charger.setName(dto.getName());
        charger.setStatus(dto.getStatus());

        charger.setCreatedBy(user.getUsername());
        charger.setCreatedDate(new Date());
        charger.setUpdatedBy(user.getUsername());
        charger.setUpdatedDate(new Date());

        chargerRepository.save(charger);

        return charger;

    }

    public Charger update(ChargerDTO dto,Authentication authentication) {

        User user = (User) authentication.getPrincipal();
        if (user == null) {
            throw new UnauthorizedException(HttpStatus.UNAUTHORIZED, "Not Authorized");
        }

        Charger charger = chargerRepository.findOneById(dto.getId());

        charger.setChargerStatusId(dto.getChargerStatusId());
        charger.setLocationId(dto.getLocationId());
        charger.setChargerImageUrl(dto.getChargerImageUrl());
        charger.setName(dto.getName());
        charger.setStatus(dto.getStatus());

        charger.setUpdatedBy(user.getUsername());
        charger.setUpdatedDate(new Date());

        chargerRepository.save(charger);

        return charger;

    }

    public void delete(
            Integer chargerId,
            Authentication authentication
    ){
        User user = (User) authentication.getPrincipal();

        if (user == null) {
            throw new BadRequestException(HttpStatus.UNAUTHORIZED, "User is UNAUTHORIZED");
        }
        chargerRepository.deleteById(chargerId);
    }
}
