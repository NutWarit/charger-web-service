package com.charger.webservice.config;

import com.charger.webservice.entity.user.User;
import com.charger.webservice.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class JdbcUserDetails implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String authenticate) throws UsernameNotFoundException {
        User user = null;

        if (authenticate != null) {
            user = userRepository.findByEmail(authenticate);
        } else {
            throw new UsernameNotFoundException("Username not found");
        }

        return user;
    }
}