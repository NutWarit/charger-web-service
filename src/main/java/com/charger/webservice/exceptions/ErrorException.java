package com.charger.webservice.exceptions;

import org.springframework.http.HttpStatus;

public class ErrorException extends CustomMVCGenericException {

    public ErrorException(HttpStatus statusCode, String msgId) {
        super(statusCode, msgId);
    }

}
