package com.charger.webservice.exceptions;

import org.springframework.http.HttpStatus;

import java.util.Locale;

/**
 * Created by suchedchaisiricharoenkul on 7/30/15 AD.
 */
public class CustomMVCGenericException extends CustomHTTPGenericException {
    public CustomMVCGenericException(HttpStatus statusCode, String msgId) {
        super(statusCode, msgId);
    }

    public CustomMVCGenericException(HttpStatus statusCode, String msgId, Locale locale) {
        super(statusCode, msgId, locale);
    }
}
