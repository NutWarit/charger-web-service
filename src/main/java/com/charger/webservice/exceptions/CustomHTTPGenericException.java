package com.charger.webservice.exceptions;

import org.springframework.http.HttpStatus;

import java.util.Locale;

/**
 * Created by suchedchaisiricharoenkul on 2/23/15 AD.
 */
public class CustomHTTPGenericException extends RuntimeException {
    private HttpStatus statusCode;
    private String msgId;
    private Locale locale;

    public CustomHTTPGenericException(HttpStatus statusCode, String msgId) {
        this(statusCode,msgId,Locale.ENGLISH);
    }

    public CustomHTTPGenericException(HttpStatus statusCode, String msgId, Locale locale) {
        super(msgId);
        this.statusCode = statusCode;
        this.msgId = msgId;
        this.locale = locale;
    }

    public HttpStatus getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(HttpStatus statusCode) {
        this.statusCode = statusCode;
    }

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

}
