package com.charger.webservice.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "payment_transaction")
public class PaymentTransaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "charger_transaction_id")
    private Integer chargerTransactionId;
    @Column(name = "invoice_no")
    private String invoiceNo;
    @Column(name = "payment_url")
    private String paymentUrl;
    @Column(name = "total_rate_kw")
    private Double totalRateKw;
    @Column(name = "total_rate_permin")
    private Double totalRatePerMin;
    @Column(name = "total_rate_kwh")
    private Double totalRateKwh;
    @Column(name = "total_time")
    private Double totalTime;
    @Column(name = "rate_cost_kwh")
    private Double rateCostKwh;
    @Column(name = "rate_cost_time")
    private Double rateCostTime;
    @Column(name = "rate_cost_start")
    private Double rateCostStart;
    @Column(name = "total_cost")
    private Double totalCost;
    @Column(name = "payment_source")
    private String paymentSource;
    @Column(name = "payment_transaction_type")
    private String paymentTransactionType;
    @Column(name = "payment_status")
    private String paymentStatus;
    @CreatedBy
    @Basic
    @Column(name = "created_by")
    private String createdBy;
    @CreatedDate
    @Temporal(TemporalType.TIMESTAMP)
    @Basic
    @Column(name = "created_date")
    private Date createdDate;
    @LastModifiedBy
    @Basic
    @Column(name = "updated_by")
    private String updatedBy;
    @LastModifiedDate
    @Temporal(TemporalType.TIMESTAMP)
    @Basic
    @Column(name = "updated_date")
    private Date updatedDate;

}
