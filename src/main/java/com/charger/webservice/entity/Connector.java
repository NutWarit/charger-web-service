package com.charger.webservice.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "connector")
public class Connector {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "charger_id")
    private Integer chargerId;
    @Column(name = "connector_type")
    private String connectorType;
    @Column(name = "connector_no")
    private Integer connectorNo;
    @Column(name = "connector_rate_kw")
    private Double connectorRateKw;
    @Column(name = "connector_rate_permin")
    private Double connectorRatePerMin;
    @Column(name = "connector_rate_price")
    private Double connectorRatePrice;
    @Column(name = "connector_rate_hours")
    private Double connectorRateHours;
    @Column(name = "connector_rate_start")
    private Double connectorRateStart;
    @Column(name = "user_token")
    private String user_token;
    @Column(name = "connector_token")
    private String connector_token;
    @Column(name = "qr_code_token")
    private String qrCodeToken;
    @Column(name = "qr_code_image_url")
    private String qrCodeImageUrl;
    @Basic
    @Column(name = "created_by")
    private String createdBy;
    @CreatedDate
    @Temporal(TemporalType.TIMESTAMP)
    @Basic
    @Column(name = "created_date")
    private Date createdDate;
    @LastModifiedBy
    @Basic
    @Column(name = "updated_by")
    private String updatedBy;
    @LastModifiedDate
    @Temporal(TemporalType.TIMESTAMP)
    @Basic
    @Column(name = "updated_date")
    private Date updatedDate;

}
