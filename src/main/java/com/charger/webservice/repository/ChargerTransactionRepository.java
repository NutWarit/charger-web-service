package com.charger.webservice.repository;

import com.charger.webservice.entity.ChargerTransaction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ChargerTransactionRepository extends JpaRepository<ChargerTransaction,Integer> {

    ChargerTransaction findOneById(Integer id);
}
