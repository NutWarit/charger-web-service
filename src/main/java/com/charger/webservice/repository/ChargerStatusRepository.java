package com.charger.webservice.repository;

import com.charger.webservice.entity.ChargerStatus;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ChargerStatusRepository extends JpaRepository<ChargerStatus,Integer> {

    ChargerStatus findOneById(Integer id);
}
