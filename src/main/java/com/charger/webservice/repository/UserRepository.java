package com.charger.webservice.repository;

import com.charger.webservice.entity.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.query.Param;

import java.util.List;

@EnableJpaRepositories
public interface UserRepository extends JpaRepository<User, Integer> {

    User findOneById(Integer id);

    User findByEmail(String email);

    User findByUsername(String username);

    User findOneByUsername(String authenticate);

    @Query("SELECT u FROM User u WHERE u.email = :email ")
    public List<User> findByEmails(@Param("email") String email);

}
