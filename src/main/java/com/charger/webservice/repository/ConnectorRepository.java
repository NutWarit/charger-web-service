package com.charger.webservice.repository;

import com.charger.webservice.entity.Connector;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ConnectorRepository extends JpaRepository<Connector,Integer> {

    Connector findOneById(Integer id);

    Connector findOneByQrCodeToken(String qrCodeToken);
}
