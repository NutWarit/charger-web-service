package com.charger.webservice.repository;

import com.charger.webservice.entity.PaymentTransaction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaymentTransactionRepository extends JpaRepository<PaymentTransaction,Integer> {

    PaymentTransaction findOneById(Integer id);
}
