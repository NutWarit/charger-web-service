package com.charger.webservice.repository;

import com.charger.webservice.entity.ResetPassword;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ResetPasswordRepository extends JpaRepository<ResetPassword, Integer> {
    ResetPassword findOneByToken(String token);
}
