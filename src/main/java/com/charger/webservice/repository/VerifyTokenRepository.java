package com.charger.webservice.repository;

import com.charger.webservice.entity.VerifyToken;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VerifyTokenRepository extends JpaRepository<VerifyToken, Integer> {
    VerifyToken findOneByTokenAndType(String token, String register);
}
