package com.charger.webservice.repository;

import com.charger.webservice.entity.Charger;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ChargerRepository extends JpaRepository<Charger,Integer> {

    Charger findOneById(Integer id);

}
