package com.charger.webservice.repository;

import com.charger.webservice.entity.Location;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LocationRepository extends JpaRepository<Location,Integer> {

    Location findOneById(Integer id);
}
