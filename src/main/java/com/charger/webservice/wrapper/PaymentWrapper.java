package com.charger.webservice.wrapper;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;

@Getter
@Setter
public class PaymentWrapper {

    private Integer id;
    private Integer chargerTransactionId;
    private Double totalRateKw;
    private Double totalRatePerMin;
    private Double totalRateKwh;
    private Double totalTime;
    private Double rateCostKwh;
    private Double rateCostTime;
    private Double rateCostStart;
    private Double totalCost;
    private String paymentSource;
    private String paymentTransactionType;
    private String paymentStatus;
}
