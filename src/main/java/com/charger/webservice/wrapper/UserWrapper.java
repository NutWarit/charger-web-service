package com.charger.webservice.wrapper;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserWrapper {
    private Integer id;
    private String username;
    private String email;
    private String profileImageUrl;
    private String firstName;
    private String lastName;
    private String cardNumber;
    private String phoneNumber;
    private String gender;
    private Integer age;
    private boolean isKyc;
    private boolean isOtp;
    private String pin;

}
