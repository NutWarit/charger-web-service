package com.charger.webservice.wrapper;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class ChargerTransactionWrapper {

    private Integer chargerTransactionId;
    private Date startTime;
    private Date stopTime;
    private String chargerTransactionStatus;
    private String qrToken;

}
