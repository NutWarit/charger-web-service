package com.charger.webservice.wrapper;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class TransactionWrapper {

    private Integer id;
    private Integer connectorId;
    private Integer userId;
    private Date startTime;
    private Date stopTime;
    private String chargerTransactionStatus;
    private String sessionId;

}
