package com.charger.webservice.controller;

import com.charger.webservice.DTO.UserDTO;
import com.charger.webservice.entity.user.User;
import com.charger.webservice.service.UserService;
import com.charger.webservice.wrapper.UserWrapper;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/manage/user")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Get Profile by Authentication", notes = "Response User Detail")
    public UserWrapper get(
            Authentication authentication
    ) {
        return userService.get(authentication);
    }

    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    public User create(
            @RequestBody UserDTO dto
    ) {
        return userService.createUser(dto);
    }

    @PutMapping("")
    @ApiOperation(value = "Update Profile by Authentication", notes = "Response User Detail")
    public User update(
            @RequestBody UserDTO dto
    ) {
        return userService.updateUser(dto);
    }

    @PostMapping("/oauth/revoke-token")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void logout(
            HttpServletRequest request
    ) {
        userService.logout(request);
    }

    @PostMapping("/verify")
    public User verifyRegisterToken(
            @RequestParam("token") String token
    ) {
        return userService.verifyRegisterToken(token);
    }

}
