package com.charger.webservice.controller;

import com.charger.webservice.DTO.LocationDTO;
import com.charger.webservice.entity.Location;
import com.charger.webservice.repository.LocationRepository;
import com.charger.webservice.service.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;

@RestController
@RequestMapping(value = "manage/location")
public class LocationController {

    @Autowired
    private LocationService locationService;

    @GetMapping("")
    @ResponseStatus(HttpStatus.OK)
    public PageImpl<Location> list(
            @RequestParam("page") int page,
            @RequestParam("size") int size,
            Authentication authentication
    ) throws ParseException {
        return locationService.list(page, size, authentication);
    }

    @GetMapping("/{locationId}")
    @ResponseStatus(HttpStatus.OK)
    public LocationDTO get(
            @PathVariable(value = "locationId") Integer locationId,
            Authentication authentication
    ) throws ParseException {
        return locationService.get(locationId, authentication);
    }

    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    public Location create(
            @RequestBody LocationDTO dto,Authentication authentication
    ) {
        return locationService.create(dto, authentication);
    }

    @PutMapping("")
    public String update(
            @RequestBody LocationDTO dto,Authentication authentication
    ) {
        return locationService.update(dto, authentication);
    }

    @DeleteMapping("/{locationId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(
            @PathVariable(value = "locationId") Integer locationId,
            Authentication authentication
    ) {
        locationService.delete(locationId, authentication);
    }
}
