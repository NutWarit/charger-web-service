package com.charger.webservice.controller;

import com.charger.webservice.DTO.ConnectorDTO;
import com.charger.webservice.DTO.edrv.RootChargeStations;
import com.charger.webservice.DTO.edrv.RootUser;
import com.charger.webservice.entity.Connector;
import com.charger.webservice.entity.user.User;
import com.charger.webservice.exceptions.BadRequestException;
import com.charger.webservice.repository.ConnectorRepository;
import com.charger.webservice.service.ConnectorService;
import com.charger.webservice.service.restservice.EdrvService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.zxing.WriterException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.text.ParseException;

@RestController
@RequestMapping(value = "manage/connector")
public class ConnectorController {

    @Autowired
    private ConnectorService connectorService;
    @Autowired
    private EdrvService edrvService;
    @Autowired
    private ConnectorRepository connectorRepository;

    @GetMapping("")
    @ResponseStatus(HttpStatus.OK)
    public PageImpl<Connector> list(
            @RequestParam("page") int page,
            @RequestParam("size") int size,
            Authentication authentication
    ) throws ParseException {
        return connectorService.list(page, size, authentication);
    }

    @GetMapping("/{connectorId}")
    @ResponseStatus(HttpStatus.OK)
    public ConnectorDTO get(
            @PathVariable(value = "connectorId") Integer connectorId,
            Authentication authentication
    ) throws ParseException {
        return connectorService.get(connectorId, authentication);
    }

    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    public Connector create(
            @RequestBody ConnectorDTO dto,Authentication authentication
    ) throws IOException, WriterException {
        String response = edrvService.edrvChargestationsById(dto);
        String reponseUser = edrvService.edrvUser();
        ObjectMapper mapper = new ObjectMapper();
        RootChargeStations responseChargeStations = mapper.readValue(response, RootChargeStations.class);
        RootUser rootUser = mapper.readValue(reponseUser, RootUser.class);
        return connectorService.create(dto, responseChargeStations, rootUser, authentication);
    }

    @PutMapping("")
    public Connector update(
            @RequestBody ConnectorDTO dto,Authentication authentication
    ) {
        return connectorService.update(dto, authentication);
    }

    @DeleteMapping("/{connectorId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(
            @PathVariable(value = "connectorId") Integer connectorId,
            Authentication authentication
    ) {
        connectorService.delete(connectorId, authentication);
    }

    @PostMapping("findByQrCodeToken")
    @ResponseStatus(HttpStatus.OK)
    public Connector findByQrCodeToken(
            @RequestBody ConnectorDTO dto,Authentication authentication
    ) {

        User user = (User) authentication.getPrincipal();

        if (user == null) {
            throw new BadRequestException(HttpStatus.UNAUTHORIZED, "User is UNAUTHORIZED");
        }

        return connectorRepository.findOneByQrCodeToken(dto.getQrCodeToken());
    }
}
