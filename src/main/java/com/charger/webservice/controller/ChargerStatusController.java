package com.charger.webservice.controller;

import com.charger.webservice.DTO.ChargerDTO;
import com.charger.webservice.DTO.ChargerStatusDTO;
import com.charger.webservice.entity.Charger;
import com.charger.webservice.entity.ChargerStatus;
import com.charger.webservice.service.ChargerStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;

@RestController
@RequestMapping(value = "manage/status")
public class ChargerStatusController {

    @Autowired
    private ChargerStatusService chargerStatusService;

    @GetMapping("")
    @ResponseStatus(HttpStatus.OK)
    public PageImpl<ChargerStatus> list(
            @RequestParam("page") int page,
            @RequestParam("size") int size,
            Authentication authentication
    ) throws ParseException {
        return chargerStatusService.list(page, size, authentication);
    }

    @GetMapping("/{statusId}")
    @ResponseStatus(HttpStatus.OK)
    public ChargerStatusDTO get(
            @PathVariable(value = "statusId") Integer statusId,
            Authentication authentication
    ) throws ParseException {
        return chargerStatusService.get(statusId, authentication);
    }

    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    public ChargerStatus create(
            @RequestBody ChargerStatusDTO dto,Authentication authentication
    ) {
        return chargerStatusService.create(dto, authentication);
    }

    @PutMapping("")
    public ChargerStatus update(
            @RequestBody ChargerStatusDTO dto,Authentication authentication
    ) {
        return chargerStatusService.update(dto, authentication);
    }

    @DeleteMapping("/{statusId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(
            @PathVariable(value = "statusId") Integer statusId,
            Authentication authentication
    ) {
        chargerStatusService.delete(statusId, authentication);
    }
}
