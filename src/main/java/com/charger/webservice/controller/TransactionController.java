package com.charger.webservice.controller;

import com.charger.webservice.DTO.*;
import com.charger.webservice.DTO.edrv.EdrvDTO;
import com.charger.webservice.DTO.edrv.RootSession;
import com.charger.webservice.DTO.edrv.SessionDTO;
import com.charger.webservice.entity.ChargerTransaction;
import com.charger.webservice.service.restservice.EdrvService;
import com.charger.webservice.service.TransactionService;
import com.charger.webservice.wrapper.ChargerTransactionWrapper;
import com.charger.webservice.wrapper.TransactionWrapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.text.ParseException;

@RestController
@RequestMapping(value = "transaction")
public class TransactionController {

    @Autowired
    EdrvService edrvService;
    @Autowired
    TransactionService transactionService;

    @GetMapping("/{transactionId}")
    @ResponseStatus(HttpStatus.OK)
    public TransactionWrapper get(
            @PathVariable(value = "transactionId") Integer transactionId,
            Authentication authentication
    ) throws ParseException {
        return transactionService.get(transactionId, authentication);
    }

    @PostMapping("/scanQRCode")
    public ChargerTransactionWrapper scanQRTransaction(
            @RequestBody ORCodeDTO orCodeDTO, Authentication authentication
    ) throws IOException {
        return transactionService.scanQRTransaction(orCodeDTO, authentication);
    }

    @PostMapping("/chargerStart/{chargerTransactionId}")
    public ChargerTransaction chargerStartTransaction(
            @PathVariable(value = "chargerTransactionId") Integer chargerTransactionId,
            @RequestBody SessionDTO dto, Authentication authentication
    ) throws JsonProcessingException {

        String response = edrvService.edrvSession(dto);
        ObjectMapper mapper = new ObjectMapper();
        RootSession responseSession = mapper.readValue(response, RootSession.class);
        String sessionId = edrvService.getSessionId(responseSession);

        return transactionService.chargerStartTransaction(chargerTransactionId, sessionId, authentication);
    }

    @PostMapping("/chargerStop/{chargerTransactionId}")
    public String chargerStopTransaction(
            @PathVariable(value = "chargerTransactionId") Integer chargerTransactionId,
            @RequestBody EdrvDTO dto, Authentication authentication
    ) {

        edrvService.edrvSessionStop(dto);

        return transactionService.chargerStopTransaction(chargerTransactionId, authentication);
    }

}
