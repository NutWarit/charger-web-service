package com.charger.webservice.controller;

import com.charger.webservice.DTO.ChargerDTO;
import com.charger.webservice.entity.Charger;
import com.charger.webservice.service.ChargerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;

@RestController
@RequestMapping(value = "manage/charger")
public class ChargerController {

    @Autowired
    private ChargerService chargerService;

    @GetMapping("")
    @ResponseStatus(HttpStatus.OK)
    public PageImpl<Charger> list(
            @RequestParam(value = "name",required = false) String name,
            @RequestParam(value = "status",required = false) String status,
            @RequestParam("page") int page,
            @RequestParam("size") int size,
            Authentication authentication
    ) throws ParseException {
        return chargerService.list(name, status, page, size, authentication);
    }

    @GetMapping("/{chargerId}")
    @ResponseStatus(HttpStatus.OK)
    public ChargerDTO get(
            @PathVariable(value = "chargerId") Integer chargerId,
            Authentication authentication
    ) throws ParseException {
        return chargerService.get(chargerId, authentication);
    }

    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    public Charger create(
            @RequestBody ChargerDTO dto,Authentication authentication
    ) {
        return chargerService.create(dto, authentication);
    }

    @PutMapping("")
    public Charger update(
            @RequestBody ChargerDTO dto,Authentication authentication
    ) {
        return chargerService.update(dto, authentication);
    }

    @DeleteMapping("/{chargerId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(
            @PathVariable(value = "chargerId") Integer chargerId,
            Authentication authentication
    ) {
        chargerService.delete(chargerId, authentication);
    }

}
