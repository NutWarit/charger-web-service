package com.charger.webservice.controller;

import com.charger.webservice.DTO.PasswordDTO;
import com.charger.webservice.DTO.UserDTO;
import com.charger.webservice.service.ResetPasswordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/password")
public class ResetPasswordController {
    @Autowired
    private ResetPasswordService resetPasswordService;

    @PostMapping("/forgot")
    @ResponseStatus(HttpStatus.OK)
    public void requestResetPassword(
            @RequestBody UserDTO dto
    ) throws Exception {
            resetPasswordService.requestResetPassword(dto);
    }

    @RequestMapping(value = "/reset", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public String resetPassword(
            @RequestBody PasswordDTO dto
            ) {
        return this.resetPasswordService.resetPassword(dto);
    }

    @RequestMapping(value = "/admin/changePassword", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public String adminChangePassword(
            @RequestBody PasswordDTO dto
    ) {
        return this.resetPasswordService.adminChangePassword(dto);
    }

    @RequestMapping(value = "/changePassword", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public String changeNewPassword(
            @RequestBody PasswordDTO dto,
            Authentication authentication
    ) {
        return this.resetPasswordService.changePassword(dto, authentication);
    }
}
