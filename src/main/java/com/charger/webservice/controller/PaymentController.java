package com.charger.webservice.controller;

import com.charger.webservice.DTO.BillPageDTO;
import com.charger.webservice.DTO.PaymentTransactionDTO;
import com.charger.webservice.DTO.edrv.EdrvDTO;
import com.charger.webservice.DTO.edrv.RootEnergyReport;
import com.charger.webservice.entity.PaymentTransaction;
import com.charger.webservice.service.restservice.EdrvService;
import com.charger.webservice.service.PaymentService;
import com.charger.webservice.wrapper.PaymentWrapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;

@RestController
@RequestMapping(value = "payment")
public class PaymentController {

    @Autowired
    EdrvService edrvService;
    @Autowired
    PaymentService paymentService;

    @GetMapping("/{paymentTransactionId}")
    @ResponseStatus(HttpStatus.OK)
    public PaymentWrapper getPayment(
            @PathVariable(value = "paymentTransactionId") Integer paymentTransactionId,
            Authentication authentication
    ) throws ParseException {
        return paymentService.getPayment(paymentTransactionId, authentication);
    }

    @PostMapping("/paymentTransaction")
    public PaymentTransaction paymentTransaction(
            @RequestBody PaymentTransactionDTO dto, Authentication authentication
    ) {
        return paymentService.paymentTransaction(dto, authentication);
    }

    @PostMapping("/calculateCost/{chargerTransactionId}")
    public PaymentTransaction calculateCost(
            @PathVariable(value = "chargerTransactionId") Integer chargerTransactionId,
            @RequestBody EdrvDTO dto, Authentication authentication
    ) throws JsonProcessingException {

        String responseReport = edrvService.edrvSessionEnergyReport(dto);
        ObjectMapper mapper = new ObjectMapper();
        RootEnergyReport responseEnergy = mapper.readValue(responseReport, RootEnergyReport.class);

        return paymentService.calculateCost(chargerTransactionId, responseEnergy, authentication);
    }

    @PostMapping("/billPayment/{paymentTransactionId}")
    public BillPageDTO billPayment(
            @PathVariable(value = "paymentTransactionId") Integer paymentTransactionId,
            Authentication authentication
    ) throws JsonProcessingException {
        return paymentService.billPayment(paymentTransactionId, authentication);
    }

}
