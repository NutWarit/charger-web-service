package com.charger.webservice.util;

import java.text.DecimalFormat;

public class Constants {

    public static class CalculateDivide {
        public static final Integer divide = 1000;
    }

    public static class format {

        public static final  DecimalFormat twoPlaces = new DecimalFormat("#,##0.00");

    }

    public static class MESSAGE_CODE_ERRORS {
        public static final String FIRSTNAMEaass_AND_LASTNAME_NOT_EMPTY = "errors.firstname_and_lastname_not_empty";
        public static final String Serial_Number_Not_Empty = "errors.serial_number_not_empty";
        public static final String Claim_Accept = "errors.claim_accept_true";
        public static final String K2_ERROR = "K2 Error";
        public static final String AZURE_AD_ERROR = "AZURE AD Error";
        public static final String USER_CLAIM_OVER_LIMIT = "คุณได้ทำการเบิกเกินจำนวนที่กำหนด";
        public static final String USER_CLAIM_OVER_AVAILABLE_COINS = "คะแนนคงเหลือไม่เพียงพอ";
        public static final String CLAIM_NOT_SHOW = "รายการนี้งดเบิกชั่วคราว.";
        public static final String LEAVE_BENEFIT_NOT_EXIST = "Leave Benefit Id not exist.";
        public static final String RELATIVE_TYPE_NOT_EXIST = "Relative type is not valid";
        public static final String MISSING_BODY = "The required body is missing";
        public static final String CLAIM_NOT_ENROLL = "User did not enroll this Claim";
        public static final String CLAIM_PROBATION = "Claim is not visible to probationer.";
        public static final String REDEEM_ERROR = "Redeem Fail.";
    }

    public static class MESSAGE_API_ERRORS {
        public static final String USER_NOT_FOUND = "User with request userId is not found.";
        public static final String USER_ID_NULL = "UserId cannot be null.";

    }
    public static class COIN_TRANSACTION_TYPE {
        public static final Integer ADD = 1;
        public static final Integer MINUS = 0;
    }

    public static class CLAIM_LIMIT_TYPE {
        public static final Integer DAY = 0;
        public static final Integer MONTH = 1;
        public static final Integer YEAR = 2;

    }
}
