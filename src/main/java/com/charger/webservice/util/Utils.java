package com.charger.webservice.util;

import javax.persistence.Query;

public class Utils {
    public final static String defaultLocale = "";

    public Utils() {
    }

    public static boolean setParameter(Query query, String name, Object value) {
        try {
            query.setParameter(name, value);
            return true;
        } catch (Exception var4) {
            return false;
        }
    }

    public static boolean isFileUrlChanged(String oldUrl, String newUrl) {
        if (oldUrl == null) {
            return false;
        } else {
            return !oldUrl.equals(newUrl);
        }
    }
}